package com.guruofjava.springbootapp.model;

public class User implements Cloneable {

    private Integer userId;
    private String name;
    private String email;
    private String remarks;

    public User() {
        super();
    }

    public void init() {
        System.out.println("User object is initialized, ID: " + userId);
    }

    public void destroy() {
        System.out.println("User object is destroyed, ID: " + userId);
    }

    public User(Integer userId, String name, String email, String remarks) {
        this.userId = userId;
        this.name = name;
        this.email = email;
        this.remarks = remarks;
    }

    @Override
    public User clone() {
        return new User(userId, name, email, remarks);
    }

    @Override
    public String toString() {
        return String.format("[ %3d | %-20s | %-30s | %-15s ]",
                userId, name, email, remarks);
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
