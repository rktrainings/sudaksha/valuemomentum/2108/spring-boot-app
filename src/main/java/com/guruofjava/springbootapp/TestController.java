package com.guruofjava.springbootapp;

import com.guruofjava.springbootapp.model.User;
import java.util.List;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    final static Logger LOG = LogManager.getLogger(TestController.class);

    @Autowired
    private User testUser;

    @Autowired
    private TestService testService;

    @RequestMapping
    public String hello() {
        //return "Welcome to Spring Boot Web Application. " + testUser;
        return "Hello";
    }

    @RequestMapping("/hello")
    public String sayHello() {
        return testService.sayHello();
    }

    @RequestMapping(value = "/users")
    public List<User> getUsers() {
        return testService.getUsers();
    }

    @RequestMapping(value = "/users", produces = {MediaType.APPLICATION_XML_VALUE})
    public List<User> getUsersXML() {
        return testService.getUsers();
    }

    @RequestMapping(value = "/users/{userId}")
    public User getUser(@PathVariable("userId") Integer userId) {
        LOG.log(Level.INFO, userId + " user accessed");

        try {
            return testService.getUserById(userId);
        } catch (EmptyResultDataAccessException ex) {
            //LOG.log(Level.WARN, userId + " user doesn't exist");
            LOG.log(Level.FATAL, "user with {} id doesn't exist", userId);
            throw ex;
        }
    }

    @ExceptionHandler(value = {EmptyResultDataAccessException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handleEmptyResult() {
    }

}
