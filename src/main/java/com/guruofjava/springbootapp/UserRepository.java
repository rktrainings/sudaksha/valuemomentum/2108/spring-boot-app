package com.guruofjava.springbootapp;

import com.guruofjava.springbootapp.model.User;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Integer getUserCount() {
        return jdbcTemplate.queryForObject("SELECT count(user_id) FROM users", Integer.class);
    }

    public void deleteUser(Integer userId) {
        jdbcTemplate.update("DELETE FROM users WHERE user_id=?", userId);
    }

    public void addUser(User user) {
        jdbcTemplate.update("INSERT INTO users VALUES(?, ?, ?, ?)",
                0, user.getName(), user.getEmail(), user.getRemarks());
    }

    public User getUserById(Integer userId) {
        return jdbcTemplate.queryForObject("SELECT * FROM users WHERE user_id=?",
                new UserRowMapper(), userId);
    }

    public List<User> getUsers() {

        return jdbcTemplate.query("SELECT * FROM users", new UserRowMapper());
    }

    public List<User> getMatchedUsers(String input) {
        return jdbcTemplate.query("SELECT * FROM users WHERE name like ?",
                new UserRowMapper(),
                input);
    }

    public User getNewUser() {
        return new User();
    }
}
