package com.guruofjava.springbootapp;

import com.guruofjava.springbootapp.model.User;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestService {

    @Autowired
    private UserRepository userRepo;

    public String sayHello() {
        return "Hello";
    }
    
    public List<User> getUsers(){
        return userRepo.getUsers();
    }
    
    public User getUserById(Integer userId){
        return userRepo.getUserById(userId);
    }
}
