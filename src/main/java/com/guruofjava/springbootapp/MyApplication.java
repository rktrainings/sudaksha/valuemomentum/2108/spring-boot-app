package com.guruofjava.springbootapp;

import com.guruofjava.springbootapp.model.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class MyApplication {

    @Bean
    public User testUser() {
        return new User(0, "Test User", "test@mail.com", "");
    }

    public static void main(String[] args) {
        SpringApplication.run(MyApplication.class, args);
    }
}
