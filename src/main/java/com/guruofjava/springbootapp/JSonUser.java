package com.guruofjava.springbootapp;

public class JSonUser {

    private Integer id;
    private String name;
    private String username;
    private String email;
    private String website;

    public JSonUser() {
        super();
    }

    @Override
    public String toString() {
        return "JSonUser{" + "id=" + id + ", name=" + name + ", username=" + username + ", email=" + email + ", website=" + website + '}';
    }

    public JSonUser(Integer id, String name, String username, String email, String website) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.email = email;
        this.website = website;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }
}
