package com.guruofjava.springbootapp;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

@Controller
public class RestTemplateController {

    @RequestMapping(value = "/login", method = {RequestMethod.GET})
    public String loginPage() {
        return "login";
    }

    @RequestMapping("/jsonuser/{userId}")
    public String getJSonUser(Model model, @PathVariable Integer userId) {

        RestTemplate template = new RestTemplate();

        JSonUser user = template
                .getForObject("https://jsonplaceholder.typicode.com/users/{userId}",
                        JSonUser.class, userId);

        model.addAttribute("user", user);

        return "viewJsonUser";
    }

    @RequestMapping("/jsonusers")
    public String getJSonUsers(Model model) {

        RestTemplate template = new RestTemplate();

        ResponseEntity<JSonUser[]> objects = template
                .getForEntity("https://jsonplaceholder.typicode.com/users",
                        JSonUser[].class);

        JSonUser[] usersObject = objects.getBody();

        model.addAttribute("users", usersObject);

        return "viewJsonUsers";
    }
}
