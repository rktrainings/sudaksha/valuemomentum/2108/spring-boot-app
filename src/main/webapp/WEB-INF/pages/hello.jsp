<%-- 
    Document   : hello
    Created on : 14-Sep-2021, 6:22:09 pm
    Author     : 
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <h2><%= java.time.LocalDateTime.now() %></h2>
    </body>
</html>
