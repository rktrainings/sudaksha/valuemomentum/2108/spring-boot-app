<%-- 
    Document   : login
    Created on : 15-Sep-2021, 8:27:11 pm
    Author     : 
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <c:if test="${param.error}">
            Login Failed: ${param.error}<br/>
        </c:if>

        <% out.println(request.getRemoteUser());%>


        <form action="/login" method="post">
            Username:
            <input type="text" name="username" size="30"/><br/>
            Password:
            <input type="password" name="password" size="30"/><br/>
            <input type="submit" value="Sign In"/>
        </form>
    </body>
</html>
