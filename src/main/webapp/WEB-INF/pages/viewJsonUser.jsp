<%-- 
    Document   : viewJsonUser
    Created on : 15-Sep-2021, 6:15:20 pm
    Author     : 
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <table border="1" cellpadding="3px" cellspacing="3px">
            <tr><td>ID</td><td>Name</td><td>Username</td><td>Email</td><td>Website</td></tr>
            <tr>
                <td>${user.id}</td>
                <td>${user.name}</td>
                <td>${user.username}</td>
                <td>${user.email}</td>
                <td>${user.website}</td>
            </tr>
        </table>
    </body>
</html>
