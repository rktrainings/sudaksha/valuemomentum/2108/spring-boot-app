package com.guruofjava.springbootapp.test;

import com.guruofjava.springbootapp.TestController;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import com.guruofjava.springbootapp.model.User;
import java.util.List;
import org.junit.jupiter.api.Disabled;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DemoTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Value("${spring.datasource.driver-class-name}")
    private String driverClassName;

    @Autowired
    private TestController testController;

    @Test
    public void testUserExists() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(List.of(MediaType.APPLICATION_JSON));
        HttpEntity entity = new HttpEntity(headers);

        ResponseEntity<User> response = restTemplate
                .exchange("http://localhost:" + port + "/users/{userId}",
                        HttpMethod.GET, entity, User.class, 1);

        User user = response.getBody();
        assertThat(user).isNotNull();
        assertEquals(user.getName(), "Tom Hanks");
    }

    @Test
    public void testHello() {
        assertThat(restTemplate.getForObject("http://localhost:" + port + "/",
                String.class)).contains("Hello");
    }

    @Test
    public void testControllerExists() {
        assertThat(testController).isNotNull();
    }

    @Test
    public void runTest() {
        System.out.println("runTest() processed, Driver: " + driverClassName);
        assertEquals(10, 10, "equal");
    }
}
