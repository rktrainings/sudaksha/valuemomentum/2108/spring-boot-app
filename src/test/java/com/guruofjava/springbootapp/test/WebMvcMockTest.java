package com.guruofjava.springbootapp.test;

import com.guruofjava.springbootapp.TestController;
import com.guruofjava.springbootapp.TestService;
import com.guruofjava.springbootapp.model.User;
import static org.hamcrest.CoreMatchers.containsString;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.when;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = {TestController.class})
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class WebMvcMockTest {

    @Autowired
    private MockMvc mvcMock;

    @MockBean
    private TestService testService;

    @Test
    public void testGetUserById() throws Exception {
        when(testService.getUserById(1)).thenReturn(new User(1, "", "", ""));

        mvcMock
                .perform(get("/users/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("{\"userId\":1}"));
    }

    @Test
    public void testHello() throws Exception {
        when(testService.sayHello()).thenReturn("Hi");

        mvcMock
                .perform(get("/hello"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Hi")));
    }
}
